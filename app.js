const resultText = document.querySelector('.text-result')
const vsText = document.querySelector('.text-vs');
const comBox = document.querySelectorAll('.com-choice.grey-box');
const playerBox = document.querySelectorAll('.player-choice');
const reset = document.querySelector('.refresh');
const player = document.querySelectorAll('.player-choice .player');
let isNotClicked = true;
let result = '';

// Get Computer Choice
function getComChoice() {
    let comp = Math.random();
    if (comp <= 0.34) return 'rock';
    if (comp <= 0.67) return 'paper';
    if (comp <= 1)return 'scissors';
}

// result of the game
function getResult(player, comp) {
    if (player === comp) return 'DRAW';
    if (player === 'rock') return (comp === 'scissors') ? 'PLAYER 1 WIN' : 'COM WIN';
    if (player === 'paper') return (comp === 'rock') ? 'PLAYER 1 WIN' : 'COM WIN';
    if (player === 'scissors') return (comp === 'paper') ? 'PLAYER 1 WIN' : 'COM WIN';
}

// load COM choice
function animationComp() {
    const start = new Date().getTime();
    let j = 0;

    setInterval(() => {
        if (new Date().getTime() - start >= 1000) {
            clearInterval;
            return;
        }

        comBox[j++].style.backgroundColor = '#c4c4c4';
        if (j === comBox.length) j = 0;

    }, 50);

    setTimeout(() => {
        setInterval(() => {
            if (new Date().getTime() - start >= 1200) {
                clearInterval;
                return;
            }

            comBox[j++].style.backgroundColor = 'transparent';
            if (j === comBox.length) j = 0;

        }, 50);
    }, 50);
}

function showResult(result) {
    resultText.classList.remove('d-none');
    vsText.classList.add('d-none');
    resultText.textContent = result;
}

function hideResult() {
    resultText.classList.add('d-none');
    vsText.classList.remove('d-none');

    for (let i = 0; i < playerBox.length; i++) {
        playerBox[i].style.backgroundColor = 'transparent';
        comBox[i].style.backgroundColor = 'transparent';
    }
}

function setBackgroundCom(result, comChoice) {
    setTimeout(() => {

        switch (comChoice) {
            case 'rock':
                comBox[0].style.backgroundColor = '#c4c4c4';
                break;
            case 'paper':
                comBox[1].style.backgroundColor = '#c4c4c4';
                break;
            case 'scissors':
                comBox[2].style.backgroundColor = '#c4c4c4';
                break;
            default:
                break;
        }

        showResult(result);
    }, 1200);
}

function resetGame() {
    reset.addEventListener('click', () => {
        hideResult();

        result = '';
        isNotClicked = true;
    })
}

function playGames() {

    player.forEach(choice => {

        choice.addEventListener('click', () => {

            if (isNotClicked) {

                const comChoice = getComChoice();
                const playerChoice = choice.className.substr(18, 8);
                result = getResult(playerChoice, comChoice);

                playerBox.forEach((p1) => {
                    p1.addEventListener('click', () => {
                        p1.style.backgroundColor = '#c4c4c4';
                    })
                });

                animationComp();
                setBackgroundCom(result, comChoice);
                isNotClicked = false;

                console.log(`Player Choose : ${playerChoice}, COM Choose : ${comChoice}, Result : ${result}`);
            } else {
                alert('Please refresh first!');

                playerBox.forEach((el) => {
                    el.addEventListener('click', () => {
                        el.style.backgroundColor = 'transparent';
                    })
                });
            }

        })
    });

    resetGame();
}

playGames();